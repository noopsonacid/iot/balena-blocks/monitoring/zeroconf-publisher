FROM balenalib/raspberrypi4-64-golang:1.17-stretch-build as builder

WORKDIR /go/src/zeroconf-publisher

COPY go.* *.go ./
RUN go mod tidy
RUN CGO_ENABLED=0 go build

FROM busybox
COPY --from=builder /go/src/zeroconf-publisher/main /usr/bin/zeroconf-publisher

ENV DOCKER_API_VERSION=1.40
CMD [ "zeroconf-publisher" ]
