package main

import (
	"context"
	"flag"
	"fmt"
	// "io"
	// "io/ioutil"
	"log"
	// "net/http"
	// "os"
	"strconv"
	"strings"
	// "syscall"
	// "time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/grandcat/zeroconf"
)

var cli *client.Client

// var hostname string

var (
	label   = flag.String("label", "io.prometheus.zeroconf.port", "Docker label uses for filtering docker containers")
	domain  = flag.String("domain", "local.", "Set the network domain. Default should be fine.")
	service = flag.String("service", "_prometheus-http._tcp", "Set the service type of the published services.")
	// waitTime = flag.Int("wait", 0, "Duration in [s] to publish service for.")
)
var Running []types.Container

func main() {
	flag.Parse()

	var err error

	// Init docker cli
	cli, err = client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	// Listen for docker events
	event := make(chan string)
	deregister := make(chan int)

	err = reload(deregister)
	if err != nil {
		panic(err)
	}

	// log.Println("Start Listen Goroutine")
	go listen(event, deregister)

	for {
		e := <-event
		log.Println("Event received: ", e)
		err = reload(deregister)
		if err != nil {
			panic(err)
		}
	}
}

// Listen for docker events
func listen(event chan string, dereg chan int) {
	log.Println("Listen containers")

	filter := filters.NewArgs()
	filter.Add("type", "container")
	filter.Add("event", "create")
	filter.Add("event", "start")
	filter.Add("event", "stop")
	filter.Add("event", "die")

	msg, errChan := cli.Events(context.Background(), types.EventsOptions{
		Filters: filter,
	})

	for {
		select {
		case err := <-errChan:
			panic(err)
		case m := <-msg:
			dereg <- 1
			log.Println("Deregister all services from mDNS...")
			event <- m.Action
		}
	}
}

// Reload the list of running containers
func reload(dereg chan int) error {

	log.Println("Reloading containers")

	filter := filters.NewArgs()
	filter.Add("label", *label)

	var err error
	Running, err = cli.ContainerList(context.Background(), types.ContainerListOptions{
		Filters: filter,
	})
	if err != nil {
		return err
	}
	for _, container := range Running {
		log.Println("Start Handler Goroutine for container ", container.Names[0])
		go handler(container, dereg)
	}
	return nil
}

func handler(container types.Container, dereg chan int) {
	// log.Println("Publish monitoring port ")
	var job string
	var id []rune
	var port int
	var txt []string

	job = strings.Split(strings.TrimPrefix(container.Names[0], "/"), "_")[0]
	id = []rune(container.ID)
	port, _ = strconv.Atoi(container.Labels[*label])
	txt = []string{fmt.Sprintf("job=%s", job)}

	server, err := zeroconf.Register(string(id[0:12]), *service, *domain, port, txt, nil)
	if err != nil {
		panic(err)
	}
	log.Printf("Published service with Type: %s, Job: %s, Port: ", job, *service, port)
	defer server.Shutdown()

	for {
		<-dereg
		log.Printf("Unpublished service with Type: %s, Job: %s, Port: ", job, *service, port)
		return
	}
}

